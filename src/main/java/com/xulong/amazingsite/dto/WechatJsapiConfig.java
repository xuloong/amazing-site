package com.xulong.amazingsite.dto;

import lombok.Data;

/**
 * WechatJsapiConfig
 *
 * @author xulong
 * @date 2018/12/3
 */
@Data
public class WechatJsapiConfig {

    private String appid;

    private String timestamp;

    private String nonceStr;

    private String signature;

    private String url;

}
