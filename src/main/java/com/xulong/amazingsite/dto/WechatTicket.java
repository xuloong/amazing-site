package com.xulong.amazingsite.dto;

import lombok.Data;

/**
 * WechatTicket
 *
 * @author xulong
 * @date 2018/12/4
 */
@Data
public class WechatTicket {

    private Integer errcode;

    private String errmsg;

    private String ticket;

    private Integer expires_in;

}
