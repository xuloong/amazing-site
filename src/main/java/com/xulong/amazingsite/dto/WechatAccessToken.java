package com.xulong.amazingsite.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * WechatAccessToken
 *
 * @author xulong
 * @date 2018/11/28
 */
@Data
public class WechatAccessToken {

    @ApiModelProperty(value = "凭证", dataType = "String")
    private String access_token;

    @ApiModelProperty(value = "凭证有效时间，单位：秒", dataType = "Integer")
    private Integer expires_in;

    @ApiModelProperty(value = "错误码", dataType = "Integer")
    private Integer errcode;

    @ApiModelProperty(value = "错误信息", dataType = "String")
    private String errmsg;

}
