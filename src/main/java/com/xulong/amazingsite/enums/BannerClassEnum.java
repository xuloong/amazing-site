package com.xulong.amazingsite.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * BannerClassEnum
 *
 * @author xulong
 * @date 2018/11/10
 */
public enum BannerClassEnum {

    CLASS1(1, "类型1"),

    CLASS2(2, "类型2"),

    CLASS3(3, "类型3");

    private int index;

    private String description;

    private static final List<BannerClassEnum> enumList = new ArrayList<BannerClassEnum>();

    static {
        for (BannerClassEnum bannerClassEnum : EnumSet.allOf(BannerClassEnum.class)) {
            enumList.add(bannerClassEnum);
        }
    }

    public static BannerClassEnum valueOf(int value) {
        switch (value) {
            case 1:
                return CLASS1;
            case 2:
                return CLASS2;
            case 3:
                return CLASS3;
            default:
                return null;
        }
    }

    BannerClassEnum(int index, String description) {
        this.index = index;
        this.description = description;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static List<BannerClassEnum> getEnumList() {
        return enumList;
    }

}
