package com.xulong.amazingsite.repository;

import com.xulong.amazingsite.model.MessageFotile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * MessageFotileRepository
 *
 * @author xulong
 * @date 2019-03-31
 */
@Repository
public interface MessageFotileRepository extends JpaRepository<MessageFotile, Long> {
}
