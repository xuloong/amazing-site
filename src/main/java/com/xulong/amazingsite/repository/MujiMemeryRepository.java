package com.xulong.amazingsite.repository;

import com.xulong.amazingsite.model.MujiMemery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * MujiMemeryRepository
 *
 * @author xulong
 * @date 2020/4/26
 */
@Repository
public interface MujiMemeryRepository extends JpaRepository<MujiMemery, Long> {
}
