package com.xulong.amazingsite.repository;

import com.xulong.amazingsite.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * JobRepository
 *
 * @author xulong
 * @date 2018/11/8
 */
@Repository
public interface JobRepository extends JpaRepository<Job, Long> {
}
