package com.xulong.amazingsite.repository;

import com.xulong.amazingsite.model.History;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * HistoryRepository
 *
 * @author xulong
 * @date 2018/11/23
 */
public interface HistoryRepository extends JpaRepository<History, Long> {
}
