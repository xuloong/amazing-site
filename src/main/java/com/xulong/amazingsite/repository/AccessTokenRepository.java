package com.xulong.amazingsite.repository;

import com.xulong.amazingsite.model.AccessToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * AccessTokenRepository
 *
 * @author xulong
 * @date 2020/5/1
 */
@Repository
public interface AccessTokenRepository extends JpaRepository<AccessToken, Long> {
}
