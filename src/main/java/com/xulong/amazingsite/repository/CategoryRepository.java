package com.xulong.amazingsite.repository;

import com.xulong.amazingsite.enums.ArticleClassEnum;
import com.xulong.amazingsite.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Category
 *
 * @author xulong
 * @date 2018/11/8
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    List<Category> findAllByArticleClassOrderBySortNum(ArticleClassEnum articleClass);

}
