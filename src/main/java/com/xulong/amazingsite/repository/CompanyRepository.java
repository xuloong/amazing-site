package com.xulong.amazingsite.repository;

import com.xulong.amazingsite.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * CompanyRepository
 *
 * @author xulong
 * @date 2018/11/9
 */
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
}
