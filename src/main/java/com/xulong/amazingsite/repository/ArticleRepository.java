package com.xulong.amazingsite.repository;

import com.xulong.amazingsite.model.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ArticleRepository
 *
 * @author xulong
 * @date 2018/9/18
 */
@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

    Page<Article> findAll(Specification specification, Pageable pageable);

    Integer countByCategoryId(Long categoryId);

}
