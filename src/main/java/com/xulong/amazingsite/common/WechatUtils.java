package com.xulong.amazingsite.common;

import com.alibaba.fastjson.JSONObject;
import com.xulong.amazingsite.dto.WechatAccessToken;
import com.xulong.amazingsite.dto.WechatJsapiConfig;
import com.xulong.amazingsite.dto.WechatTicket;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * WechatUtils
 *
 * @author xulong
 * @date 2018/12/3
 */
public class WechatUtils {

    public static WechatJsapiConfig getConfig(String url, String appId, String jsapiTicket) throws Exception {
        WechatJsapiConfig config = new WechatJsapiConfig();
        config.setAppid(appId);
        String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
        config.setTimestamp(timestamp);
        String nonceStr = String.valueOf(ThreadLocalRandom.current().nextInt(89999999) + 10000000);
        config.setNonceStr(nonceStr);
        String signature = getSignature(jsapiTicket, nonceStr, timestamp, url);
        config.setSignature(signature);
        config.setUrl(url);
        return config;
    }

    public static String getSignature(String jsapi_ticket, String noncestr, String timestamp, String url) {
        Map<String, String> params = new HashMap();
        params.put("jsapi_ticket", jsapi_ticket);
        params.put("noncestr", noncestr);
        params.put("timestamp", timestamp);
        params.put("url", url);
        Map<String, String> sortParams = sortAsc(params);
        String string1 = mapJoin(sortParams, false);

        try {
            return DigestUtils.sha1Hex(string1.getBytes("UTF-8"));
        } catch (IOException var8) {
            return "";
        }
    }

    public static Map<String, String> sortAsc(Map<String, String> map) {
        HashMap<String, String> tempMap = new LinkedHashMap();
        List<Map.Entry<String, String>> infoIds = new ArrayList(map.entrySet());
        Collections.sort(infoIds, new Comparator<Map.Entry<String, String>>() {
            public int compare(Map.Entry<String, String> o1, Map.Entry<String, String> o2) {
                return ((String) o1.getKey()).compareTo((String) o2.getKey());
            }
        });

        for (int i = 0; i < infoIds.size(); ++i) {
            Map.Entry<String, String> item = (Map.Entry) infoIds.get(i);
            tempMap.put(item.getKey(), item.getValue());
        }

        return tempMap;
    }

    public static String mapJoin(Map<String, String> map, boolean valueUrlEncode) {
        StringBuilder sb = new StringBuilder();
        Iterator i$ = map.keySet().iterator();

        while (true) {
            String key;
            do {
                do {
                    if (!i$.hasNext()) {
                        if (sb.length() > 0) {
                            sb.deleteCharAt(sb.length() - 1);
                        }

                        return sb.toString();
                    }

                    key = (String) i$.next();
                } while (map.get(key) == null);
            } while ("".equals(map.get(key)));

            try {
                String temp = key.endsWith("_") && key.length() > 1 ? key.substring(0, key.length() - 1) : key;
                sb.append(temp);
                sb.append("=");
                String value = (String) map.get(key);
                if (valueUrlEncode) {
                    value = URLEncoder.encode((String) map.get(key), "utf-8").replace("+", "%20");
                }

                sb.append(value);
                sb.append("&");
            } catch (UnsupportedEncodingException var7) {
                var7.printStackTrace();
            }
        }
    }

    public static WechatAccessToken getAccessToken(String appId, String appSecret) {

        String url = new StringBuffer(Constant.WECHAT_URL_TOKEN)
                .append("?appid=")
                .append(appId)
                .append("&secret=")
                .append(appSecret)
                .append("&grant_type=client_credential").toString();

        String jsonResult = HttpClient4.doGet(url);
        WechatAccessToken wechatAccessToken = JSONObject.parseObject(jsonResult, WechatAccessToken.class);

        return wechatAccessToken;

    }

    public static WechatTicket getTicket(String accessToken) {

        String url = new StringBuffer(Constant.WECHAT_URL_JSAPI_TICKET)
                .append("?access_token=")
                .append(accessToken)
                .append("&type=jsapi").toString();

        String jsonResult = HttpClient4.doGet(url);
        WechatTicket wechatTicket = JSONObject.parseObject(jsonResult, WechatTicket.class);

        return wechatTicket;

    }

}
