package com.xulong.amazingsite.common;

public class Constant {

	/**
	 * OAuth2.0中网站资源ID
	 */
	public static String OAUTH2_RESOURCE_ID = "amazing_site";

    public static String OAUTH2_CLIENT_ID1 = "api";

    public static String OAUTH2_CLIENT_ID1_SECRET = "amazing";

    public static String OAUTH2_CLIENT_ID2 = "web";

    public static String OAUTH2_CLIENT_ID2_SECRET = "amazing";

    public static final int OAUTH2_ACCESS_TOKEN_VALIDITY_SECONDS = 7200;

    public static final int OAUTH2_REFRESH_TOKEN_VALIDITY_SECONDS = 7200;

    public static String OAUTH2_TOKEN_NAME = "Authorization";

    public static String OAUTH2_TOKEN_TYPE = "bearer";

    /**
     * 微信登录凭证校验接口地址
     */
    public static String WECHAT_URL_CODE2SESSION = "https://api.weixin.qq.com/sns/jscode2session";

    public static String WECHAT_APP_ID_1BUO = "wx9f932b25341f2305";

    public static String WECHAT_APP_SECRET_1BUO = "4f58af4af02d0df5cb79981771ef7497";

    public static String WECHAT_APP_ID_MUJI = "wx4ce2ed9ca645e7b9";

    public static String WECHAT_APP_SECRET_MUJI = "8135a8b8984ffb4ff43a8ee736c6e3dd";

    /**
     * 获取小程序全局唯一后台接口调用凭据接口地址
     */
    public static String WECHAT_URL_TOKEN = "https://api.weixin.qq.com/cgi-bin/token";

    /**
     * 获取小程序码接口地址
     */
    public static String WECHAT_URL_CODE = "https://api.weixin.qq.com/wxa/getwxacodeunlimit";

    /**
     * 获取公众号用于调用微信JS接口的临时票据接口地址
     */
    public static String WECHAT_URL_JSAPI_TICKET = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";

}
