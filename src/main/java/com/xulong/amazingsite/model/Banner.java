package com.xulong.amazingsite.model;

import com.xulong.amazingsite.enums.ArticleClassEnum;
import com.xulong.amazingsite.enums.BannerClassEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Banner
 *
 * @author xulong
 * @date 2018/8/5
 */
@Entity
@Table(name = "banner")
@Data
public class Banner {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @ApiModelProperty(value = "ID", dataType = "Long")
    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(value = "类型(1:首页首屏Banner;2.首页下方Banner;)", dataType = "BannerClassEnum", example = "CLASS1")
    private BannerClassEnum bannerClass;

    @Column(nullable = false)
    @ApiModelProperty(value = "标题", dataType = "String", example = "我是标题")
    private String title;

    @ApiModelProperty(value = "摘要", dataType = "String", example = "我是摘要")
    private String summary;

    @ApiModelProperty(value = "图片链接", dataType = "String")
    private String imageUrl;

    @ApiModelProperty(value = "链接", dataType = "String")
    private String link;

    @Column(nullable = false)
    @ApiModelProperty(value = "状态(1:有效;0:无效)", dataType = "Integer", example = "1")
    private Integer status = 1;

    @Column(nullable = false)
    @ApiModelProperty(value = "排序数", dataType = "Integer", example = "0")
    private Integer sortNum = 0;

}
