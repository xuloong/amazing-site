package com.xulong.amazingsite.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * MessageFotile
 *
 * @author xulong
 * @date 2019-03-31
 */
@Entity
@Table(name = "message_fotile")
@EntityListeners(AuditingEntityListener.class)
@Data
public class MessageFotile {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @ApiModelProperty(value = "ID", dataType = "Long")
    private Long id;

    @Column(nullable = false)
    @ApiModelProperty(value = "姓名", dataType = "String", example = "张三")
    private String name;

    @Column(nullable = false)
    @ApiModelProperty(value = "电话", dataType = "String", example = "13888888888")
    private String tel;

    @ApiModelProperty(value = "省份", dataType = "String", example = "浙江")
    private String province;

    @ApiModelProperty(value = "城市", dataType = "String", example = "宁波")
    private String city;

    @ApiModelProperty(value = "区县", dataType = "String", example = "杭州湾新区")
    private String area;

    @Column(length = 5000)
    @ApiModelProperty(value = "地址", dataType = "String", example = "南京路")
    private String content;

    @CreatedDate
    @ApiModelProperty(value = "创建时间（不用赋值）", dataType = "Date", example = "2018-01-01 00:00:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
