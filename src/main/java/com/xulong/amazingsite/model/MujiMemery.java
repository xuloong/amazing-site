package com.xulong.amazingsite.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * MujiMemery
 *
 * @author xulong
 * @date 2020/4/26
 */
@Entity
@Table(name = "muji_memery")
@EntityListeners(AuditingEntityListener.class)
@Data
public class MujiMemery {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @ApiModelProperty(value = "ID", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "微信OpenId", dataType = "String")
    private String openId;

    @ApiModelProperty(value = "姓名", dataType = "String", example = "张三")
    private String name;

    @ApiModelProperty(value = "电话", dataType = "String", example = "13888888888")
    private String tel;

    @ApiModelProperty(value = "头像链接", dataType = "String")
    private String headImageUrl;

    @ApiModelProperty(value = "图片链接", dataType = "String")
    private String imageUrl;

    @Column(columnDefinition = "MEDIUMTEXT")
    @ApiModelProperty(value = "图片Base64", dataType = "String")
    private String imageBase64;

    @Column(length = 5000)
    @ApiModelProperty(value = "内容", dataType = "String", example = "Hello World")
    private String content;

    @CreatedDate
    @ApiModelProperty(value = "创建时间（不用赋值）", dataType = "Date", example = "2018-01-01 00:00:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "点赞数", dataType = "Integer")
    private Integer likes = 0;

}
