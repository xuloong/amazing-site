package com.xulong.amazingsite.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Job
 *
 * @author xulong
 * @date 2018/11/7
 */
@Entity
@Table(name = "job")
@EntityListeners(AuditingEntityListener.class)
@Data
public class Job {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @ApiModelProperty(value = "ID", dataType = "Long")
    private Long id;

    @Column(nullable = false)
    @ApiModelProperty(value = "岗位名称", dataType = "String", example = "我是岗位名称")
    private String name;

    @Column(nullable = false)
    @ApiModelProperty(value = "招聘人数", dataType = "String", example = "2")
    private String num;

    @Column(nullable = false)
    @ApiModelProperty(value = "工作地点", dataType = "String", example = "上海")
    private String place;

    @Column(nullable = false)
    @ApiModelProperty(value = "最低学历", dataType = "String", example = "本科")
    private String education;

    @Column(nullable = false)
    @ApiModelProperty(value = "工作经验", dataType = "String", example = "3年")
    private String experience;

    @Column(nullable = false, columnDefinition = "TEXT")
    @ApiModelProperty(value = "工作职责", dataType = "String")
    private String duty;

    @Column(nullable = false, columnDefinition = "TEXT")
    @ApiModelProperty(value = "任职要求", dataType = "String")
    private String requirement;

    @CreatedDate
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @CreatedBy
    private Long creater;

    @LastModifiedDate
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifiedTime;

    @LastModifiedBy
    private Long modifier;

}
