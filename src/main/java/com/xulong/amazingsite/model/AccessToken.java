package com.xulong.amazingsite.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * AccessToken
 *
 * @author xulong
 * @date 2020/5/1
 */
@Entity
@Table(name = "access_token")
@EntityListeners(AuditingEntityListener.class)
@Data
public class AccessToken {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    private String accessToken;

    private Date expiresIn;

}
