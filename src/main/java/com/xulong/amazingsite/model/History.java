package com.xulong.amazingsite.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * History
 *
 * @author xulong
 * @date 2018/11/23
 */
@Entity
@Table(name = "history")
@Data
public class History {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @ApiModelProperty(value = "ID", dataType = "Long")
    private Long id;

    @Column(nullable = false)
    @ApiModelProperty(value = "事迹内容", dataType = "String", example = "我是事迹内容")
    private String summary;

    @Column(nullable = false)
    @ApiModelProperty(value = "年份", dataType = "Integer", example = "我是年份")
    private Integer year;

    @Column(nullable = false)
    @ApiModelProperty(value = "月份", dataType = "Integer", example = "我是月份")
    private Integer month;

    @Column(nullable = false)
    @ApiModelProperty(value = "排序数", dataType = "Integer", example = "0")
    private Integer sortNum = 0;

}
