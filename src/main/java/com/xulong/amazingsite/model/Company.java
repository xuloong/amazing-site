package com.xulong.amazingsite.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Company
 *
 * @author xulong
 * @date 2018/11/9
 */
@Entity
@Table(name = "company")
@Data
public class Company {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @ApiModelProperty(value = "ID", dataType = "Long")
    private Long id;

    @Column(nullable = false)
    @ApiModelProperty(value = "公司名称", dataType = "String", example = "我是公司名称")
    private String name;

    @ApiModelProperty(value = "公司地址", dataType = "String", example = "我是公司地址")
    private String address;

    @ApiModelProperty(value = "公司精度(百度地图)", dataType = "String", example = "我是公司精度")
    private String longitude;

    @ApiModelProperty(value = "公司纬度(百度地图)", dataType = "String", example = "我是公司纬度")
    private String latitude;

    @ApiModelProperty(value = "公司电话", dataType = "String", example = "我是公司电话")
    private String tel;

    @ApiModelProperty(value = "公司邮箱", dataType = "String", example = "我是公司邮箱")
    private String email;

    @ApiModelProperty(value = "ICP备案号", dataType = "String", example = "我是ICP备案号")
    private String icp;

    @ApiModelProperty(value = "招聘邮箱", dataType = "String", example = "我是招聘邮箱")
    private String jobEmail;

    @ApiModelProperty(value = "LOGO链接", dataType = "String")
    private String logoUrl;

    @ApiModelProperty(value = "公司简介", dataType = "String", example = "我是公司简介")
    private String summary;

}
