package com.xulong.amazingsite.model;

import com.xulong.amazingsite.enums.ArticleClassEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Dictionary
 *
 * @author xulong
 * @date 2018/11/7
 */
@Entity
@Table(name = "category")
@Data
public class Category {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @ApiModelProperty(value = "ID", dataType = "Long")
    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(value = "类别(1:新闻;2.产品;3.关于我们;4.下载;5.服务;6.方案)", dataType = "ArticleClassEnum", example = "NEWS")
    private ArticleClassEnum articleClass = ArticleClassEnum.NEWS;

    @Column(nullable = false)
    @ApiModelProperty(value = "分类名称", dataType = "String", example = "我是分类名称")
    private String name;

    @Column(nullable = false)
    @ApiModelProperty(value = "排序数", dataType = "Integer", example = "0")
    private Integer sortNum = 0;

}
