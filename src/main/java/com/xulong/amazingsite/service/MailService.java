package com.xulong.amazingsite.service;

import org.springframework.mail.MailException;

/**
 * MailService
 *
 * @author xulong
 * @date 2020/7/2
 */
public interface MailService {

    void sendSimpleMail(String to, String subject, String content);

}
