package com.xulong.amazingsite.service;

import com.xulong.amazingsite.base.BaseService;
import com.xulong.amazingsite.enums.ArticleClassEnum;
import com.xulong.amazingsite.model.Category;

import java.util.List;

/**
 * CategoryService
 *
 * @author xulong
 * @date 2018/11/8
 */
public interface CategoryService extends BaseService<Category> {

    List<Category> getList(ArticleClassEnum articleClass);

}
