package com.xulong.amazingsite.service;

import com.xulong.amazingsite.base.BaseService;
import com.xulong.amazingsite.enums.ArticleClassEnum;
import com.xulong.amazingsite.model.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * ArticleService
 *
 * @author xulong
 * @date 2018/9/19
 */
public interface ArticleService extends BaseService<Article> {

    Page<Article> getList(ArticleClassEnum articleClass, Long categoryId, Integer showIndex, Pageable pageable);

    Integer getCountByCategoryId(Long categoryId);

}
