package com.xulong.amazingsite.service;

import com.xulong.amazingsite.base.BaseService;
import com.xulong.amazingsite.model.History;

/**
 * HistoryService
 *
 * @author xulong
 * @date 2018/11/23
 */
public interface HistoryService extends BaseService<History> {
}
