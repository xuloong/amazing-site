package com.xulong.amazingsite.service;

import com.xulong.amazingsite.base.BaseService;
import com.xulong.amazingsite.enums.BannerClassEnum;
import com.xulong.amazingsite.model.Banner;

import java.util.List;

/**
 * BannerService
 *
 * @author xulong
 * @date 2018/11/8
 */
public interface BannerService extends BaseService<Banner> {

    List<Banner> getList(BannerClassEnum bannerClass);

}
