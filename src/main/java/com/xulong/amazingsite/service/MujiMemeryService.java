package com.xulong.amazingsite.service;

import com.xulong.amazingsite.base.BaseService;
import com.xulong.amazingsite.model.MujiMemery;

/**
 * MujiMemeryService
 *
 * @author xulong
 * @date 2020/4/26
 */
public interface MujiMemeryService extends BaseService<MujiMemery> {
}
