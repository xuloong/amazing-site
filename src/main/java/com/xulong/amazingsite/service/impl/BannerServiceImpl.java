package com.xulong.amazingsite.service.impl;

import com.xulong.amazingsite.base.impl.BaseServiceImpl;
import com.xulong.amazingsite.enums.BannerClassEnum;
import com.xulong.amazingsite.model.Banner;
import com.xulong.amazingsite.repository.BannerRepository;
import com.xulong.amazingsite.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * BannerServiceImpl
 *
 * @author xulong
 * @date 2018/11/8
 */
@Service
public class BannerServiceImpl extends BaseServiceImpl<Banner> implements BannerService {

    @Autowired
    private BannerRepository bannerRepository;

    @Override
    public List<Banner> getList(BannerClassEnum bannerClass) {
        return bannerRepository.findAllByBannerClassOrderBySortNum(bannerClass);
    }
}
