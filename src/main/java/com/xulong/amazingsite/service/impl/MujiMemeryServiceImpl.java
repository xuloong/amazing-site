package com.xulong.amazingsite.service.impl;

import com.xulong.amazingsite.base.impl.BaseServiceImpl;
import com.xulong.amazingsite.model.MujiMemery;
import com.xulong.amazingsite.service.MujiMemeryService;
import org.springframework.stereotype.Service;

/**
 * MujiMemeryServiceImpl
 *
 * @author xulong
 * @date 2020/4/26
 */
@Service
public class MujiMemeryServiceImpl extends BaseServiceImpl<MujiMemery> implements MujiMemeryService {
}
