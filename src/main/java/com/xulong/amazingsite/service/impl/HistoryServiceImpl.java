package com.xulong.amazingsite.service.impl;

import com.xulong.amazingsite.base.impl.BaseServiceImpl;
import com.xulong.amazingsite.model.History;
import com.xulong.amazingsite.repository.HistoryRepository;
import com.xulong.amazingsite.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * HistoryServiceImpl
 *
 * @author xulong
 * @date 2018/11/23
 */
@Service
public class HistoryServiceImpl extends BaseServiceImpl<History> implements HistoryService {
}
