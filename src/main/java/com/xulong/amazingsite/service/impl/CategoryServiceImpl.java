package com.xulong.amazingsite.service.impl;

import com.xulong.amazingsite.base.impl.BaseServiceImpl;
import com.xulong.amazingsite.enums.ArticleClassEnum;
import com.xulong.amazingsite.model.Category;
import com.xulong.amazingsite.repository.CategoryRepository;
import com.xulong.amazingsite.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * CategoryServiceImpl
 *
 * @author xulong
 * @date 2018/11/9
 */
@Service
public class CategoryServiceImpl extends BaseServiceImpl<Category> implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> getList(ArticleClassEnum articleClass) {

        return categoryRepository.findAllByArticleClassOrderBySortNum(articleClass);

    }

}
