package com.xulong.amazingsite.service.impl;

import com.xulong.amazingsite.base.impl.BaseServiceImpl;
import com.xulong.amazingsite.enums.ArticleClassEnum;
import com.xulong.amazingsite.model.Article;
import com.xulong.amazingsite.repository.ArticleRepository;
import com.xulong.amazingsite.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * ArticleServiceImpl
 *
 * @author xulong
 * @date 2018/9/19
 */
@Service
public class ArticleServiceImpl extends BaseServiceImpl<Article> implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Override
    public Page<Article> getList(ArticleClassEnum articleClass, Long categoryId, Integer showIndex, Pageable pageable) {

        Specification specification = new Specification<Article>() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {

                List<Predicate> predicates = new ArrayList<>();
                if (articleClass != null) {
                    predicates.add(criteriaBuilder.equal(root.get("articleClass"), articleClass));
                }
                if (categoryId != null) {
                    predicates.add(criteriaBuilder.equal(root.get("categoryId"), categoryId));
                }
                if (showIndex != null) {
                    predicates.add(criteriaBuilder.equal(root.get("showIndex"), showIndex));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        return  articleRepository.findAll(specification, pageable);

    }

    @Override
    public Integer getCountByCategoryId(Long categoryId) {
        return articleRepository.countByCategoryId(categoryId);
    }

}
