package com.xulong.amazingsite.service.impl;

import com.xulong.amazingsite.base.impl.BaseServiceImpl;
import com.xulong.amazingsite.model.MessageFotile;
import com.xulong.amazingsite.repository.MessageFotileRepository;
import com.xulong.amazingsite.service.MessageFotileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * MessageFotileServiceImpl
 *
 * @author xulong
 * @date 2019-03-31
 */
@Service
public class MessageFotileServiceImpl extends BaseServiceImpl<MessageFotile> implements MessageFotileService {

    @Autowired
    private MessageFotileRepository messageFotileRepository;

    @Override
    public Long getCount() {
        return messageFotileRepository.count();
    }

}
