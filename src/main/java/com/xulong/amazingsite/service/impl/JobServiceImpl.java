package com.xulong.amazingsite.service.impl;

import com.xulong.amazingsite.base.impl.BaseServiceImpl;
import com.xulong.amazingsite.model.Job;
import com.xulong.amazingsite.service.JobService;
import org.springframework.stereotype.Service;

/**
 * JobServiceImpl
 *
 * @author xulong
 * @date 2018/11/9
 */
@Service
public class JobServiceImpl extends BaseServiceImpl<Job> implements JobService {
}
