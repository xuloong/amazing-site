package com.xulong.amazingsite.service.impl;

import com.xulong.amazingsite.base.impl.BaseServiceImpl;
import com.xulong.amazingsite.model.Company;
import com.xulong.amazingsite.service.CompanyService;
import org.springframework.stereotype.Service;

/**
 * CompanyServiceImpl
 *
 * @author xulong
 * @date 2018/11/9
 */
@Service
public class CompanyServiceImpl extends BaseServiceImpl<Company> implements CompanyService {
}
