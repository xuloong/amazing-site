package com.xulong.amazingsite.service;

import com.xulong.amazingsite.base.BaseService;
import com.xulong.amazingsite.model.Job;

/**
 * JobService
 *
 * @author xulong
 * @date 2018/11/8
 */
public interface JobService extends BaseService<Job> {
}
