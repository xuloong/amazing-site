package com.xulong.amazingsite.service;

import com.xulong.amazingsite.base.BaseService;
import com.xulong.amazingsite.model.Company;

/**
 * CompanyService
 *
 * @author xulong
 * @date 2018/11/9
 */
public interface CompanyService extends BaseService<Company> {
}
