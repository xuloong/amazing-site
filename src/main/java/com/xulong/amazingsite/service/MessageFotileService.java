package com.xulong.amazingsite.service;

import com.xulong.amazingsite.base.BaseService;
import com.xulong.amazingsite.model.MessageFotile;

/**
 * MessageFotileService
 *
 * @author xulong
 * @date 2019-03-31
 */
public interface MessageFotileService extends BaseService<MessageFotile> {

    Long getCount();

}
