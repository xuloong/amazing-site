package com.xulong.amazingsite.base;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * BaseService
 *
 * @author xulong
 * @date 2018/9/19
 */
public interface BaseService<T> {

    public T getById(Long id);

    public T save(T entity);

    public void delete(Long id);

    public List<T> getAll(Sort sort);

    public Page<T> getList(Pageable pageable);

}
