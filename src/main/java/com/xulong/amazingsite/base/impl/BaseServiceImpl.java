package com.xulong.amazingsite.base.impl;

import com.xulong.amazingsite.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * BaseServiceImpl
 *
 * @author xulong
 * @date 2018/9/19
 */
public class BaseServiceImpl<T> implements BaseService<T> {

    @Autowired
    private JpaRepository<T, Long> jpaRepository;

    @Override
    public T getById(Long id) {
        return jpaRepository.findOne(id);
    }

    @Override
    public T save(T entity) {
        return jpaRepository.save(entity);
    }

    @Override
    public void delete(Long id) {
        jpaRepository.delete(id);
    }

    @Override
    public List<T> getAll(Sort sort) {
        return jpaRepository.findAll(sort);
    }

    @Override
    public Page<T> getList(Pageable pageable) {
        return jpaRepository.findAll(pageable);
    }

}

