package com.xulong.amazingsite.controller;

import com.xulong.amazingsite.enums.BannerClassEnum;
import com.xulong.amazingsite.model.Banner;
import com.xulong.amazingsite.service.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * BannerController
 *
 * @author xulong
 * @date 2018/9/30
 */
@RestController
@RequestMapping(value = "/banners")
@Api(tags = "Banner APIs", description = "Banner接口")
public class BannerController {

    @Autowired
    private BannerService bannerService;

    @ApiOperation(value = "查询Banner详情API", httpMethod = "GET", notes = "根据ID查询Banner详情", response = Banner.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "BannerID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @GetMapping(value = "/{id}")
    public ResponseEntity<Banner> getById(@NotNull @PathVariable("id") Long id) {

        Banner banner = bannerService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(banner);

    }

    @ApiOperation(value = "查询Banner列表API", httpMethod = "GET", notes = "查询Banner列表", response = Banner.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bannerClass", value = "类型", required = false, paramType = "query", dataType = "BannerClassEnum")
    })
    @ResponseBody
    @GetMapping(value = "")
    public ResponseEntity<List<Banner>> getList(@RequestParam(value = "bannerClass") BannerClassEnum bannerClass) {

        List<Banner> banners = bannerService.getList(bannerClass);
        return ResponseEntity.status(HttpStatus.OK).body(banners);

    }

    @ApiOperation(value = "新增BannerAPI", httpMethod = "POST", notes = "新增Banner", response = Banner.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "banner", value = "Banner对象", required = true, paramType = "body", dataType = "Banner")
    })
    @ResponseBody
    @PostMapping(value = "")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Banner> create(@RequestBody Banner banner) {

        banner = bannerService.save(banner);
        return ResponseEntity.status(HttpStatus.CREATED).body(banner);

    }

    @ApiOperation(value = "修改BannerAPI", httpMethod = "PATCH", notes = "修改Banner")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "BannerID", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "banner", value = "Banner对象", required = true, paramType = "body", dataType = "Banner")
    })
    @ResponseBody
    @PatchMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public void update(@NotNull @PathVariable("id") Long id, @RequestBody Banner banner) {

        banner.setId(id);
        bannerService.save(banner);

    }

    @ApiOperation(value = "删除BannerAPI", httpMethod = "DELETE", notes = "删除Banner")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "BannerID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_USER')")
    public void delete(@NotNull @PathVariable("id") Long id) {

        bannerService.delete(id);

    }
}
