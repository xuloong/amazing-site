package com.xulong.amazingsite.controller;

import com.xulong.amazingsite.model.Company;
import com.xulong.amazingsite.service.CompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * CompanyController
 *
 * @author xulong
 * @date 2018/11/10
 */
@RestController
@RequestMapping(value = "/companies")
@Api(tags = "Company APIs", description = "公司接口")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @ApiOperation(value = "查询公司详情API", httpMethod = "GET", notes = "根据ID查询公司详情", response = Company.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "公司ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @GetMapping(value = "/{id}")
    public ResponseEntity<Company> getById(@NotNull @PathVariable("id") Long id) {

        Company company = companyService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(company);

    }

    @ApiOperation(value = "修改公司API", httpMethod = "PATCH", notes = "修改公司")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "公司ID", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "company", value = "公司对象", required = true, paramType = "body", dataType = "Company")
    })
    @ResponseBody
    @PatchMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public void update(@NotNull @PathVariable("id") Long id, @RequestBody Company company) {

        company.setId(id);
        companyService.save(company);

    }

}
