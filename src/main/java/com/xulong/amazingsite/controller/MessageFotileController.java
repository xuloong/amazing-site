package com.xulong.amazingsite.controller;

import com.xulong.amazingsite.model.MessageFotile;
import com.xulong.amazingsite.service.MailService;
import com.xulong.amazingsite.service.MessageFotileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * MessageFotileController
 *
 * @author xulong
 * @date 2019-03-31
 */
@RestController
@RequestMapping(value = "/fotile/messages")
@Api(tags = "Fotile Message APIs", description = "方太留言接口")
public class MessageFotileController {

    @Autowired
    private MessageFotileService messageFotileService;

    @Autowired
    private MailService mailService;

    @ApiOperation(value = "查询留言详情API", httpMethod = "GET", notes = "根据ID查询留言详情", response = MessageFotile.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "留言ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<MessageFotile> getById(@NotNull @PathVariable("id") Long id) {

        MessageFotile message = messageFotileService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(message);

    }

    @ApiOperation(value = "查询留言列表API", httpMethod = "GET", notes = "查询留言列表", response = Page.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页数", required = false, paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "size", value = "每页条数", required = false, paramType = "query", dataType = "Integer")
    })
    @ResponseBody
    @GetMapping(value = "")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Page<MessageFotile>> getList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                       @RequestParam(value = "size", required = false, defaultValue = "10") Integer size) {

        Pageable pageable = new PageRequest(page - 1, size, Sort.Direction.DESC, "id");
        Page<MessageFotile> messages = messageFotileService.getList(pageable);
        return ResponseEntity.status(HttpStatus.OK).body(messages);

    }

    @ApiOperation(value = "新增留言API", httpMethod = "POST", notes = "新增留言", response = MessageFotile.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "message", value = "留言对象", required = true, paramType = "body", dataType = "MessageFotile")
    })
    @ResponseBody
    @PostMapping(value = "")
    public ResponseEntity<MessageFotile> create(@RequestBody MessageFotile message) {

        message = messageFotileService.save(message);

        String to = "jiangwb@fotile.com";

        if (message.getCity().equals("北京") || message.getArea().equals("三河市")
                || message.getArea().equals("香河县") || message.getArea().equals("大厂")) {
            to = "daixg@fotile.com";
        }
        else if (message.getCity().equals("南通") || message.getCity().equals("泰州")) {
            to = "qiansc@fotile.com";
        }
        else if (message.getCity().equals("苏州") || message.getCity().equals("无锡")) {
            to = "18262725541@163.com";
        }
        String subject = "收到一条新的水质检测客资信息";
        String content = "管理员您好：";
        content += "\n收到一条新的水质检测客资信息如下：";
        content += "\n姓名：" + message.getName();
        content += "\n手机：" + message.getTel();
        content += "\n省份：" + message.getProvince();
        content += "\n城市：" + message.getCity();
        content += "\n区县：" + message.getArea();
        content += "\n请及时跟进回访，安排水质检测。";
        mailService.sendSimpleMail(to, subject, content);

        return ResponseEntity.status(HttpStatus.CREATED).body(message);

    }

    @ApiOperation(value = "查询留言数量API", httpMethod = "GET", notes = "查询留言数量", response = Long.class)
    @ResponseBody
    @GetMapping(value = "/count")
    public ResponseEntity<Long> getCount() {

        Long count = messageFotileService.getCount() + 5000;
        return ResponseEntity.status(HttpStatus.OK).body(count);

    }

}
