package com.xulong.amazingsite.controller;

import com.xulong.amazingsite.common.BizException;
import com.xulong.amazingsite.enums.ArticleClassEnum;
import com.xulong.amazingsite.model.Category;
import com.xulong.amazingsite.service.ArticleService;
import com.xulong.amazingsite.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * CategoryController
 *
 * @author xulong
 * @date 2018/11/10
 */
@RestController
@RequestMapping(value = "/categories")
@Api(tags = "Category APIs", description = "分类接口")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ArticleService articleService;

    @ApiOperation(value = "查询分类详情API", httpMethod = "GET", notes = "根据ID查询分类详情", response = Category.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "分类ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @GetMapping(value = "/{id}")
    public ResponseEntity<Category> getById(@NotNull @PathVariable("id") Long id) {

        Category category = categoryService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(category);

    }

    @ApiOperation(value = "查询分类列表API", httpMethod = "GET", notes = "查询分类列表", response = Category.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "articleClass", value = "类型", required = false, paramType = "query", dataType = "ArticleClassEnum")
    })
    @ResponseBody
    @GetMapping(value = "")
    public ResponseEntity<List<Category>> getList(@RequestParam(value = "articleClass") ArticleClassEnum articleClass) {

        List<Category> categories = categoryService.getList(articleClass);
        return ResponseEntity.status(HttpStatus.OK).body(categories);

    }

    @ApiOperation(value = "新增分类API", httpMethod = "POST", notes = "新增分类", response = Category.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category", value = "分类对象", required = true, paramType = "body", dataType = "Category")
    })
    @ResponseBody
    @PostMapping(value = "")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Category> create(@RequestBody Category category) {

        category = categoryService.save(category);
        return ResponseEntity.status(HttpStatus.CREATED).body(category);

    }

    @ApiOperation(value = "修改分类API", httpMethod = "PATCH", notes = "修改分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "文章ID", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "category", value = "分类对象", required = true, paramType = "body", dataType = "Category")
    })
    @ResponseBody
    @PatchMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public void update(@NotNull @PathVariable("id") Long id, @RequestBody Category category) {

        category.setId(id);
        categoryService.save(category);

    }

    @ApiOperation(value = "删除分类API", httpMethod = "DELETE", notes = "删除分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "分类ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_USER')")
    public void delete(@NotNull @PathVariable("id") Long id) throws BizException {

        Integer num = articleService.getCountByCategoryId(id);
        if (num > 0) {
            throw new BizException("该分类下有内容，暂不能删除");
        }

        categoryService.delete(id);

    }

}
