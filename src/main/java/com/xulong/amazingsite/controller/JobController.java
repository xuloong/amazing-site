package com.xulong.amazingsite.controller;

import com.xulong.amazingsite.model.Job;
import com.xulong.amazingsite.service.JobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * JobController
 *
 * @author xulong
 * @date 2018/11/10
 */
@RestController
@RequestMapping(value = "/jobs")
@Api(tags = "Job APIs", description = "招聘接口")
public class JobController {

    @Autowired
    private JobService jobService;

    @ApiOperation(value = "查询招聘详情API", httpMethod = "GET", notes = "根据ID查询招聘详情", response = Job.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "招聘ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @GetMapping(value = "/{id}")
    public ResponseEntity<Job> getById(@NotNull @PathVariable("id") Long id) {

        Job job = jobService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(job);

    }

    @ApiOperation(value = "查询招聘列表API", httpMethod = "GET", notes = "查询招聘列表", response = Page.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页数", required = false, paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "size", value = "每页条数", required = false, paramType = "query", dataType = "Integer")
    })
    @ResponseBody
    @GetMapping(value = "")
    public ResponseEntity<Page<Job>> getList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                 @RequestParam(value = "size", required = false, defaultValue = "10") Integer size) {

        Pageable pageable = new PageRequest(page - 1, size, Sort.Direction.DESC, "id");
        Page<Job> jobs = jobService.getList(pageable);
        return ResponseEntity.status(HttpStatus.OK).body(jobs);

    }

    @ApiOperation(value = "新增招聘API", httpMethod = "POST", notes = "新增招聘", response = Job.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "job", value = "招聘对象", required = true, paramType = "body", dataType = "Job")
    })
    @ResponseBody
    @PostMapping(value = "")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Job> create(@RequestBody Job job) {

        job = jobService.save(job);
        return ResponseEntity.status(HttpStatus.CREATED).body(job);

    }

    @ApiOperation(value = "修改招聘API", httpMethod = "PATCH", notes = "修改招聘")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "招聘ID", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "job", value = "招聘对象", required = true, paramType = "body", dataType = "Job")
    })
    @ResponseBody
    @PatchMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public void update(@NotNull @PathVariable("id") Long id, @RequestBody Job job) {

        job.setId(id);
        jobService.save(job);

    }

    @ApiOperation(value = "删除招聘API", httpMethod = "DELETE", notes = "删除招聘")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "招聘ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_USER')")
    public void delete(@NotNull @PathVariable("id") Long id) {

        jobService.delete(id);

    }

}
