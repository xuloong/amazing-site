package com.xulong.amazingsite.controller;

import com.xulong.amazingsite.model.History;
import com.xulong.amazingsite.service.HistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * HistoryController
 *
 * @author xulong
 * @date 2018/11/23
 */
@RestController
@RequestMapping(value = "/histories")
@Api(tags = "History APIs", description = "大事记接口")
public class HistoryController {

    @Autowired
    private HistoryService historyService;

    @ApiOperation(value = "查询大事记详情API", httpMethod = "GET", notes = "根据ID查询大事记详情", response = History.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "大事记ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @GetMapping(value = "/{id}")
    public ResponseEntity<History> getById(@NotNull @PathVariable("id") Long id) {

        History history = historyService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(history);

    }

    @ApiOperation(value = "查询大事记列表API", httpMethod = "GET", notes = "查询大事记列表", response = History.class)
    @ResponseBody
    @GetMapping(value = "")
    public ResponseEntity<List<History>> getList() {

        Sort sort = new Sort(Sort.Direction.ASC, "sortNum");
        List<History> histories = historyService.getAll(sort);
        return ResponseEntity.status(HttpStatus.OK).body(histories);

    }

    @ApiOperation(value = "新增大事记API", httpMethod = "POST", notes = "新增大事记", response = History.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "history", value = "大事记对象", required = true, paramType = "body", dataType = "History")
    })
    @ResponseBody
    @PostMapping(value = "")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<History> create(@RequestBody History history) {

        history = historyService.save(history);
        return ResponseEntity.status(HttpStatus.CREATED).body(history);

    }

    @ApiOperation(value = "修改大事记API", httpMethod = "PATCH", notes = "修改大事记")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "大事记ID", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "history", value = "大事记对象", required = true, paramType = "body", dataType = "History")
    })
    @ResponseBody
    @PatchMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public void update(@NotNull @PathVariable("id") Long id, @RequestBody History history) {

        history.setId(id);
        historyService.save(history);

    }

    @ApiOperation(value = "删除大事记API", httpMethod = "DELETE", notes = "删除大事记")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "大事记ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_USER')")
    public void delete(@NotNull @PathVariable("id") Long id) {

        historyService.delete(id);

    }

}
