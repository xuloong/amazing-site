package com.xulong.amazingsite.controller;

import com.xulong.amazingsite.common.Constant;
import com.xulong.amazingsite.common.WechatUtils;
import com.xulong.amazingsite.dto.WechatAccessToken;
import com.xulong.amazingsite.dto.WechatJsapiConfig;
import com.xulong.amazingsite.dto.WechatTicket;
import com.xulong.amazingsite.model.AccessToken;
import com.xulong.amazingsite.repository.AccessTokenRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;

/**
 * WechatController
 *
 * @author xulong
 * @date 2019-03-31
 */
@RestController
@RequestMapping(value = "/wechat")
@Api(tags = "Wechat APIs", description = "微信接口")
public class WechatController {

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    @ApiOperation(value = "获取公众号JSAPI配置API", httpMethod = "GET", notes = "获取公众号JSAPI配置接口", response = WechatJsapiConfig.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "url", value = "当前页面URL", paramType = "query", dataType = "String")
    })
    @ResponseBody
    @GetMapping(value = "/jsapi_config")
    public ResponseEntity<WechatJsapiConfig> getWechatJsapiConfig(@RequestParam("url") String url) throws Exception {

        WechatAccessToken wechatAccessToken;
        AccessToken accessToken = accessTokenRepository.findOne((long) 1);
        Date date = new Date();
        if (accessToken == null || accessToken.getExpiresIn().before(date)) {
            wechatAccessToken = WechatUtils.getAccessToken(Constant.WECHAT_APP_ID_MUJI, Constant.WECHAT_APP_SECRET_MUJI);
            if (accessToken == null) {
                accessToken = new AccessToken();
            }
            accessToken.setAccessToken(wechatAccessToken.getAccess_token());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.SECOND, 7000);
            accessToken.setExpiresIn(calendar.getTime());
            accessTokenRepository.save(accessToken);
        }

        WechatTicket wechatTicket = WechatUtils.getTicket(accessToken.getAccessToken());
        WechatJsapiConfig wechatJsapiConfig = WechatUtils.getConfig(url, Constant.WECHAT_APP_ID_MUJI, wechatTicket.getTicket());
        return ResponseEntity.status(HttpStatus.OK).body(wechatJsapiConfig);

    }

}
