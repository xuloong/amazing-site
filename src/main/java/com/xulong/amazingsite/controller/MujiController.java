package com.xulong.amazingsite.controller;

import com.xulong.amazingsite.common.Base64Utils;
import com.xulong.amazingsite.model.MujiMemery;
import com.xulong.amazingsite.service.MujiMemeryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * MujiController
 *
 * @author xulong
 * @date 2020/4/26
 */
@RestController
@RequestMapping(value = "/muji")
@Api(tags = "MUJI APIs", description = "MUJI接口")
public class MujiController {

    @Value("${amazing.config.upload-file-path-muji}")
    private String UPLOAD_FILE_PATH;
    @Value("${amazing.config.view-file-path-muji}")
    private String VIEW_FILE_PATH;
    @Value("${amazing.config.view-file-domain-muji}")
    private String VIEW_FILE_DOMAIN;

    @Autowired
    private MujiMemeryService mujiMemeryService;

    @ApiOperation(value = "新增回忆API", httpMethod = "POST", notes = "新增回忆", response = MujiMemery.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memery", value = "回忆对象", required = true, paramType = "body", dataType = "MujiMemery")
    })
    @ResponseBody
    @PostMapping(value = "/memeries")
    public ResponseEntity<MujiMemery> create(@RequestBody MujiMemery memery) {

        String fileId = UUID.randomUUID().toString();
        String imgFilePath = UPLOAD_FILE_PATH + fileId + ".png";
        Base64Utils.Base64ToImage(memery.getImageBase64(), imgFilePath);
        memery.setImageUrl(VIEW_FILE_DOMAIN + VIEW_FILE_PATH + fileId + ".png");
        memery.setImageBase64("");
        memery = mujiMemeryService.save(memery);
        return ResponseEntity.status(HttpStatus.CREATED).body(memery);

    }

    @ApiOperation(value = "查询回忆详情API", httpMethod = "GET", notes = "根据ID查询回忆详情", response = MujiMemery.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "回忆ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @GetMapping(value = "/memeries/{id}")
    public ResponseEntity<MujiMemery> getById(@NotNull @PathVariable("id") Long id) {

        MujiMemery memery = mujiMemeryService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(memery);

    }

    @ApiOperation(value = "查询回忆列表API", httpMethod = "GET", notes = "查询回忆列表", response = Page.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页数", required = false, paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "size", value = "每页条数", required = false, paramType = "query", dataType = "Integer")
    })
    @ResponseBody
    @GetMapping(value = "/memeries")
    public ResponseEntity<Page<MujiMemery>> getList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                       @RequestParam(value = "size", required = false, defaultValue = "10") Integer size) {

        Pageable pageable = new PageRequest(page - 1, size, Sort.Direction.DESC, "likes");
        Page<MujiMemery> memeries = mujiMemeryService.getList(pageable);
        return ResponseEntity.status(HttpStatus.OK).body(memeries);

    }

    @ApiOperation(value = "回忆点赞API", httpMethod = "PATCH", notes = "回忆点赞")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "回忆ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @PatchMapping(value = "/memeries/{id}/like")
    public void like(@NotNull @PathVariable("id") Long id) {

        MujiMemery memery = mujiMemeryService.getById(id);
        memery.setLikes(memery.getLikes() + 1);
        mujiMemeryService.save(memery);

    }

    @ApiOperation(value = "回忆取消点赞API", httpMethod = "DELETE", notes = "回忆取消点赞")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "回忆ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @DeleteMapping(value = "/memeries/{id}/like")
    public void dislike(@NotNull @PathVariable("id") Long id) {

        MujiMemery memery = mujiMemeryService.getById(id);
        if (memery.getLikes() > 0) {
            memery.setLikes(memery.getLikes() - 1);
            mujiMemeryService.save(memery);
        }

    }

    @ApiOperation(value = "删除回忆API", httpMethod = "DELETE", notes = "删除回忆")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "回忆ID", required = true, paramType = "path", dataType = "Long")
    })
    @ResponseBody
    @DeleteMapping(value = "/memeries/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_USER')")
    public void delete(@NotNull @PathVariable("id") Long id) {

        mujiMemeryService.delete(id);

    }

}
