package com.xulong.amazingsite;

import com.xulong.amazingsite.model.Article;
import com.xulong.amazingsite.repository.ArticleRepository;
import com.xulong.amazingsite.repository.UserRepository;
import com.xulong.amazingsite.model.User;
import com.xulong.amazingsite.service.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AmazingSiteApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private MailService mailService;

    @Test
    public void test() throws Exception {

        String to = "xulongq@qq.com";
        String subject = "收到一条新的水质检测客资信息";
        String content = "管理员您好：\n";
        content += "收到一条新的水质检测客资信息如下：\n";
        content += "姓名：张三\n";
        content += "手机：13888888888\n";
        content += "省份：江苏\n";
        content += "城市：苏州\n";
        content += "区县：相城区\n";
        content += "请及时跟进回访，安排水质检测。";
        mailService.sendSimpleMail(to, subject, content);

//        userRepository.save(new User("系统管理员", "admin", "123456"));
//
//        User user = userRepository.findByName("张三");
//        user.setName("张三三");
//        userRepository.save(user);

//        // 测试findAll, 查询所有记录
//        Assert.assertEquals(10, userRepository.findAll().size());
//
//        // 测试findByName, 查询姓名为FFF的User
//        Assert.assertEquals(60, userRepository.findByName("FFF").getAge().longValue());
//
//        // 测试findUser, 查询姓名为FFF的User
//        Assert.assertEquals(60, userRepository.findUser("FFF").getAge().longValue());
//
//        // 测试findByNameAndAge, 查询姓名为FFF并且年龄为60的User
//        Assert.assertEquals("FFF", userRepository.findByNameAndAge("FFF", 60).getName());
//
//        // 测试删除姓名为AAA的User
//        userRepository.delete(userRepository.findByName("AAA"));
//
//        // 测试findAll, 查询所有记录, 验证上面的删除是否成功
//        Assert.assertEquals(9, userRepository.findAll().size());

    }

}
